package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	
	ArrayList<FridgeItem> itemInFridge;
	int maxItems = 20;
	int numItems;
	
	
	
	public Fridge() {
		this.itemInFridge = new ArrayList<FridgeItem>();
		this.numItems = 0;
		
		
	}

	@Override
	public int nItemsInFridge() {
		return this.numItems;
	}

	@Override
	public int totalSize() {
		
		return maxItems;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (this.numItems >= this.maxItems) {
				return false;
		}
		this.itemInFridge.add(item);
		this.numItems ++;
		return true;
	}

	@Override
	public void takeOut(FridgeItem item) {
		//if (this.numItems == 0) {
			
		//}
		if (this.itemInFridge.remove(item)) {
			this.numItems --;
		}
		else {
			throw new NoSuchElementException() ;
		}
		
		

	}

	@Override
	public void emptyFridge() {
		this.itemInFridge.clear();
		this.numItems = 0;

	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		
		List<FridgeItem> expiredItems = new ArrayList<>();
		
		//System.out.println(this.nItemsInFridge());
		
		//int currentNumberItems = this.nItemsInFridge();
		
		// Not the best, but use double loop. First, find the outdated items.
		for (int i = 0; i < this.nItemsInFridge(); i++) {
			//FridgeItem temp = new FridgeItem(this.itemInFridge.get(i));
			FridgeItem temp = this.itemInFridge.get(i);
			//System.out.println(temp.toString());
			
			if (temp.hasExpired()) {
				//System.out.println("something has expired for integer, " + i);
			
				//System.out.println(this.itemInFridge.get(i).toString());
				//FridgeItem expiredItems = new FridgeItem(this.itemInFridge.get(i));
				FridgeItem expiredItem = new FridgeItem(this.itemInFridge.get(i).getName(), this.itemInFridge.get(i).getExpirationDate());
				expiredItems.add(expiredItem);
				//this.takeOut(expiredItem);
			}
			//FridgeItem expiredItem = new FridgeItem(this.itemInFridge.get(i), );
			//expiredItems.add(expiredItem);
			//fridge.placeIn(expiredItem);
		}

		
		//System.out.println("finnished first loop");
		
		// Next, remove all the outdated items from fridge.
		for (int i = 0; i < expiredItems.size(); i++) {
			FridgeItem itemToRemove = expiredItems.get(i);
			this.takeOut(itemToRemove);
		}
		
		return expiredItems;
	}

}
